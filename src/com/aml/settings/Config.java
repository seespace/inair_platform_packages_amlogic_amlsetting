package com.aml.settings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import android.app.SystemWriteManager;


import android.content.Context;
import android.util.Log;
import android.os.SystemProperties;

public class Config {
	private static final boolean mDebug = false;
    private static final String TAG = "Config";

	public static void Logd(String msg1, String msg2) {
		if (mDebug) {
			Log.d(msg1, msg2);
		}
	}

	public static void Loge(String msg1, String msg2) {
		if (mDebug) {
			Log.e(msg1, msg2);
		}
	}

	public static String mDisplayOutputEntries[] = null;
	public static String mLogicOutputEntries[] = null;

	public static final void Instance(Context context) {
		if ((mDisplayOutputEntries != null) && (mLogicOutputEntries != null)) {
			return;
		}

		if(DisplayFragment.isDongle){
				mDisplayOutputEntries = context.getResources().getStringArray(
				R.array.outputmode_entries_display_dongle);
				mLogicOutputEntries = context.getResources().getStringArray(
				R.array.outputmode_entries_logic_dongle);
		}else{
				mDisplayOutputEntries = context.getResources().getStringArray(
				R.array.outputmode_entries_display);
				mLogicOutputEntries = context.getResources().getStringArray(
				R.array.outputmode_entries_logic);
		}

		int index = 0;

		Logd("*************************", "*************************");
		for (index = 0; index < mDisplayOutputEntries.length; index++) {
			Logd("Config", "mDisplayOutputEntries[" + index + "] is: "
					+ mDisplayOutputEntries[index]);
		}
		Logd("*************************", "*************************");
		for (index = 0; index < mLogicOutputEntries.length; index++) {
			Logd("Config", "mLogicOutputEntries[" + index + "] is: "
					+ mLogicOutputEntries[index]);
		}
		Logd("*************************", "*************************");
	}

	public static final String Display2LogicEntry(String displayEntry) {
		int index = 0;

		if ((displayEntry != null) && (mDisplayOutputEntries != null)
				&& (mLogicOutputEntries != null)) {
			for (index = 0; index < mDisplayOutputEntries.length; index++) {
				if (displayEntry.equalsIgnoreCase(mDisplayOutputEntries[index])) {
					return mLogicOutputEntries[index];
				}
			}
		}

		return null;
	}

	public static final String Logic2DisplayEntry(String logicEntry) {
		int index = 0;

		if ((logicEntry != null) && (mDisplayOutputEntries != null)
				&& (mLogicOutputEntries != null)) {
			for (index = 0; index < mLogicOutputEntries.length; index++) {
				if (logicEntry.equalsIgnoreCase(mLogicOutputEntries[index])) {
					return mDisplayOutputEntries[index];
				}
			}
		}

		return null;
	}

	public final static String mCurrentResolution = "/sys/class/display/mode";

    public static String getCurrentOutputResolution(SystemWriteManager sw){
        if(sw==null){
            return null;
        }
        String mode =  sw.readSysfs(mCurrentResolution);
        if ("480cvbs".equalsIgnoreCase(mode)) {
            mode = "480i";
        } else if ("576cvbs".equalsIgnoreCase(mode)) {
            mode = "576i";
        }
        return mode;
    }

    public static String getCurrentOutputResolution(Context c){
        if(c==null)  return null;
        SystemWriteManager sw = (SystemWriteManager) c.getSystemService("system_write");
        if(sw==null) return null;
        String mode = sw.readSysfs(mCurrentResolution);
        if ("480cvbs".equalsIgnoreCase(mode)) {
            mode = "480i";
        } else if ("576cvbs".equalsIgnoreCase(mode)) {
            mode = "576i";
        }
        return mode;
    }

    /*
	public static String getCurrentOutputResolution() {
		String currentMode = null;

		FileReader fileReader = null;
		try {
			fileReader = new FileReader(mCurrentResolution);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		BufferedReader bufferedReader = null;
		bufferedReader = new BufferedReader(fileReader);

		try {
			currentMode = bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (currentMode.equalsIgnoreCase("480cvbs")) {
			currentMode = "480i";
		} else if (currentMode.equalsIgnoreCase("576cvbs")) {
			currentMode = "576i";
		}

		return currentMode;
	}
    */

	// public static final String[] Display2LogicEntries(String[]
	// displayEntries)
	// {
	// int index = 0;
	//
	// String[] logicEntries = null;
	//
	// if((displayEntries != null) && (mDisplayOutputEntries != null) &&
	// (mLogicOutputEntries != null))
	// {
	// logicEntries = new String[displayEntries.length];
	//
	// for(index = 0; index < displayEntries.length; index++)
	// {
	// logicEntries[index] = Display2LogicEntry(displayEntries[index]);
	// }
	//
	// return logicEntries;
	// }
	//
	// return null;
	// }
	//
	// public static final String[] Logic2DisplayEntries(String[] logicEntries)
	// {
	// int index = 0;
	//
	// String[] displayEntries = null;
	//
	// if((logicEntries != null) && (mDisplayOutputEntries != null) &&
	// (mLogicOutputEntries != null))
	// {
	// displayEntries = new String[logicEntries.length];
	//
	// for(index = 0; index < logicEntries.length; index++)
	// {
	// displayEntries[index] = Logic2DisplayEntry(logicEntries[index]);
	// }
	//
	// return displayEntries;
	// }
	//
	// return null;
	// }

    public static boolean isMBXDevice(){
            String mode = SystemProperties.get("ro.platform.has.mbxuimode");
            if("true".equalsIgnoreCase(mode))
            {
                    return true;
            }
                    
            return false;
    }

}
