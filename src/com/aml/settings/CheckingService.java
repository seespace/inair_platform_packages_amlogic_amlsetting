package com.aml.settings;

import java.io.File;
import java.util.Locale;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.Service;
import android.app.SystemWriteManager;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UEventObserver;
import android.util.Log;

public class CheckingService extends Service {
	private SystemWriteManager sw = null;
	private static final String TAG = "CheckingService";
	private static final String mAutoLanguagePreferencesFile = "cec_language_preferences";
	private static final String PROP_CEC_LANGUAGE_AUTO_SWITCH = "auto.swtich.language.by.cec";
	private static final String KEY_AUTO_LANGUAGE_SWITCH = "AutoLanguageSwitch";

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sw = (SystemWriteManager) getSystemService("system_write");
		startListenCecDev();
	}

	private void startListenCecDev() {
		Log.d(TAG, "startListenCecDev()");
		if (new File("/sys/devices/virtual/switch/lang_config/state").exists()) {
			mCedObserver.startObserving("DEVPATH=/devices/virtual/switch/lang_config");
			// final String filename = "/sys/class/switch/lang_config/state";
		}
	}

	private UEventObserver mCedObserver = new UEventObserver() {
		@Override
        public void onUEvent(UEventObserver.UEvent event) {
            Log.d(TAG, "onUEvent()");
            SharedPreferences sharedpreference = getSharedPreferences(
            mAutoLanguagePreferencesFile, Context.MODE_PRIVATE);

            Boolean isNeedAutoSwitchLanguage = sharedpreference.getBoolean(
            KEY_AUTO_LANGUAGE_SWITCH, false);
            if (!isNeedAutoSwitchLanguage) {
                Log.d(TAG,"auto switch function if off now !!!");
                return;
            }
            String mNewLanguage = event.get("SWITCH_STATE");
            Log.d(TAG,"get the language code is : " + mNewLanguage);
            int i = -1;
            String[] cec_language_list = getResources().getStringArray(
            R.array.cec_language);
            for(int j=0 ; j < cec_language_list.length ; j++){
                    //Log.d(TAG,"===== cec_language_list[" + j + "] = " + cec_language_list[j]);
                    if(mNewLanguage!=null && mNewLanguage.trim().equals(cec_language_list[j])){
                    i = j ;
                    break;
                }
            }
            if(i>=0){
                String able = getResources().getConfiguration().locale.getCountry();
                String[] language_list = getResources().getStringArray(R.array.language);
                String[] country_list = getResources().getStringArray(R.array.country);
                if (able.equals(country_list[i])){ 
                    Log.d(TAG,"no need to change language");
                    return;
                }
                else { 
                    Locale l = new Locale(language_list[i], country_list[i]);
                    Log.d(TAG,"change the language right now !!!");
                    updateLocale(l);
                }
            }else{
                Log.d(TAG,"the language code is not support right now !!!");
            }
        }
	};

    private void updateLocale(Locale locale) {
        Log.d(TAG, "updateLocale()");
        try {
            IActivityManager am = ActivityManagerNative.getDefault();
            Configuration config = am.getConfiguration();
            config.locale = locale;
            config.userSetLocale = true;
            am.updateConfiguration(config);
            // Trigger the dirty bit for the Settings Provider.
            BackupManager.dataChanged("com.android.providers.settings");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}