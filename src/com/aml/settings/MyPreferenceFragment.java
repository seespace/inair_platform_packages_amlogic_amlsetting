package com.aml.settings;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.SystemWriteManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;
import android.widget.TextView;
import android.preference.PreferenceFrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.view.Gravity;
import android.view.View.OnKeyListener;
import android.view.KeyEvent;

public class MyPreferenceFragment extends PreferenceFragment implements
		Preference.OnPreferenceChangeListener , View.OnFocusChangeListener{

	private final String TAG = "MyPreferenceFragment";
	private static MyPreferenceFragment mInstance = null;

	Preference checkboxPref = null;

	/** If there is no setting in the provider, use this. */
	private static final int FALLBACK_SCREEN_TIMEOUT_VALUE = 30000;

	private SwitchPreference mEasyLinkPreference;
	private SwitchPreference mOneTouchPlayPreference;
	private SwitchPreference mOneTouchStandyByPreference;
	private SwitchPreference mAutoPowerOnFromTvPreference;
	private SwitchPreference cec_auto_language_switch;

	private static final String mAutoLanguagePreferencesFile = "cec_language_preferences";
	private static final String STR_CECCONFIG = "ubootenv.var.cecconfig";

	private static final String writeCecConfig = "/sys/class/amhdmitx/amhdmitx0/cec_config";
	private static final String KEY_EASYLINK = "easylink_all";
	private static final String KEY_ONE_TOUCH_PLAY = "OneTouchPlay";
	private static final String KEY_ONE_TOUCH_STANDBY = "OneTouchStandby";
	private static final String KEY_AUTO_LANGUAGE_SWITCH = "AutoLanguageSwitch";
	private static final String AUTO_LANGUAGE_SWITCH = "auto_language_switch";
	private static final String KEY_AUTO_POWERON_FROM_TV = "AutoPoweronfromTV";
	private SystemWriteManager sw = null;
    AlertDialog dialog  ;
    private TextView tipsView = null;
    

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sw = (SystemWriteManager) getActivity()
				.getSystemService("system_write");
		getActivity().getString(R.string.on);
		getActivity().getString(R.string.off);

		initEasyLinkView();

	}

    @Override
    public void onStart() {        
        super.onStart();
        getListView().setOnFocusChangeListener(this);
    }
    
	@Override
	public void onPause() {

		super.onPause();
	}

	@Override
	public void onStop() {

		super.onStop();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

            LinearLayout v = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);

            tipsView = new TextView(getActivity().getApplicationContext());
            tipsView.setTextSize(18);
            tipsView.setText(" ");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.BOTTOM ;
            params.setMargins(100, 0, 100, 100);
            tipsView.setLayoutParams(params);

            v.addView(tipsView);
            v.setOnFocusChangeListener(this);           
                        
            return v;
	}

	public static MyPreferenceFragment newInstance() {

		MyPreferenceFragment mInstance = new MyPreferenceFragment();

		return mInstance;
	}

	public static MyPreferenceFragment getInstance() {

		return mInstance;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);

	}

	private void initEasyLinkView() {
		addPreferencesFromResource(R.xml.easy_link_settings);
		// ContentResolver resolver = getActivity().getContentResolver();

		mEasyLinkPreference = (SwitchPreference) findPreference(KEY_EASYLINK);
		mEasyLinkPreference.setOnPreferenceChangeListener(this);
		mOneTouchPlayPreference = (SwitchPreference) findPreference(KEY_ONE_TOUCH_PLAY);
		mOneTouchPlayPreference.setOnPreferenceChangeListener(this);
		mOneTouchStandyByPreference = (SwitchPreference) findPreference(KEY_ONE_TOUCH_STANDBY);
		mOneTouchStandyByPreference.setOnPreferenceChangeListener(this);
		cec_auto_language_switch = (SwitchPreference) findPreference(KEY_AUTO_LANGUAGE_SWITCH);
		cec_auto_language_switch.setOnPreferenceChangeListener(this);

		updateUI();

		if (!isMyServiceRunning()) {
			Intent serviceIntent = new Intent(getActivity(),
					CheckingService.class);
			serviceIntent.setAction("CEC_LANGUAGE_AUTO_SWITCH");
			getActivity().startService(serviceIntent);
		}

	}

	private void updateUI() {
		String cec_config = sw.getPropertyString(STR_CECCONFIG, "");
        String writeConfig = null ;
		if ("".equals(cec_config)) {
			sw.setProperty(STR_CECCONFIG, "cec0");
			Log.d(TAG, "use default cecconfig:cec0");
			mEasyLinkPreference.setChecked(false);
			mEasyLinkPreference.setEnabled(true);
			setAllEnable(false);
			setAllCheck(false);
		} else {
			if (cec_config.length() == 4) {
				String configString = getBinaryString(cec_config);
				if (configString.charAt(2) == '1') {
					mOneTouchPlayPreference.setSummary(R.string.on);
					mOneTouchPlayPreference.setChecked(true);
				} else {
					mOneTouchPlayPreference.setSummary(R.string.off);
					mOneTouchPlayPreference.setChecked(false);
				}
				if (configString.charAt(1) == '1') {
					mOneTouchStandyByPreference.setSummary(R.string.on);
					mOneTouchStandyByPreference.setChecked(true);
				} else {
					mOneTouchStandyByPreference.setSummary(R.string.off);
					mOneTouchStandyByPreference.setChecked(false);
				}

				SharedPreferences sharedpreference = getActivity()
						.getSharedPreferences(mAutoLanguagePreferencesFile,
								Context.MODE_PRIVATE);
				Boolean lan = sharedpreference.getBoolean(
						KEY_AUTO_LANGUAGE_SWITCH, false);
				if(lan){
					cec_auto_language_switch.setSummary(R.string.on);
				}else{
					cec_auto_language_switch.setSummary(R.string.off);
				}
				cec_auto_language_switch.setChecked(lan);

				if (configString.charAt(0) == '1') {
					// mAutoPowerOnFromTvPreference.setSummary(R.string.easylink_status_on);
					// mAutoPowerOnFromTvPreference.setChecked(true);
				} else {
					// mAutoPowerOnFromTvPreference.setSummary(R.string.easylink_status_OFF);
					// mAutoPowerOnFromTvPreference.setChecked(false);
				}

    			if (configString.charAt(3) == '1') {
					mEasyLinkPreference.setSummary(R.string.on);
					mEasyLinkPreference.setChecked(true);
				} else {
					mEasyLinkPreference.setSummary(R.string.off);
					mEasyLinkPreference.setChecked(false);
					setAllSummary(R.string.off);
					setAll(false);
				}

			}

		}

	}

	private void setAll(boolean able) {
		mOneTouchPlayPreference.setEnabled(able);
		mOneTouchPlayPreference.setChecked(able);
		mOneTouchStandyByPreference.setEnabled(able);
		mOneTouchStandyByPreference.setChecked(able);
		cec_auto_language_switch.setEnabled(able);
		cec_auto_language_switch.setChecked(able);
	}

	private void setAllCheck(boolean able) {
		mOneTouchPlayPreference.setChecked(able);
		mOneTouchStandyByPreference.setChecked(able);
		cec_auto_language_switch.setChecked(able);
	}

	private void setAllEnable(boolean able) {
		mOneTouchPlayPreference.setEnabled(able);
		mOneTouchStandyByPreference.setEnabled(able);
		cec_auto_language_switch.setEnabled(able);
	}

	private void setAllSummary(int res) {
		mEasyLinkPreference.setSummary(res);
		mOneTouchPlayPreference.setSummary(res);
		mOneTouchStandyByPreference.setSummary(res);
		cec_auto_language_switch.setSummary(res);
	}

	private boolean isMyServiceRunning() {

		ActivityManager manager = (ActivityManager) getActivity()
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.aml.settings.CheckingService".equals(service.service
					.getClassName())) {
				return true;
			}
		}
		return false;
	}

    String setValueAtIndex(String str,int index,char value){
        char[] array = str.toCharArray();
        if(index < str.length())
        {
            array[index] = value;
        }
        else{
            Log.d(TAG,"Error : the index is bigger than length");
        }
        return String.valueOf(array);
    }

	public String getBinaryString(String config) {
		String indexString = "0123456789abcdef";
		String configString = config.substring(config.length() - 1,
				config.length());
		int indexOfConfigNum = indexString.indexOf(configString);
		String ConfigBinary = Integer.toBinaryString(indexOfConfigNum);
		if (ConfigBinary.length() < 4) {
			for (int i = ConfigBinary.length(); i < 4; i++) {
				ConfigBinary = "0" + ConfigBinary;
			}
		}
		return ConfigBinary;
	}

	public int[] getBinaryArray(String binaryString) {
		int[] tmp = new int[4];
		for (int i = 0; i < binaryString.length(); i++) {
			String tmpString = String.valueOf(binaryString.charAt(i));
			tmp[i] = Integer.parseInt(tmpString);
		}
		return tmp;
	}

	public String arrayToString(int[] array) {
		String getIndexString = "0123456789abcdef";
		int total = 0;
		System.out.println();
		for (int i = 0; i < array.length; i++) {
			total = total
					+ (int) (array[i] * Math.pow(2, array.length - i - 1));
		}
		Log.d(TAG, "in arrayToString cecConfig is:" + total);
		String cecConfig = "cec" + getIndexString.charAt(total);
		Log.d(TAG, "in arrayToString cecConfig is:" + cecConfig);
		return cecConfig;
	}

	public void writeFile(String file, String value) {
		Log.d(TAG, "Write path : " + file + ",  value : " + value);
		if (sw != null)
			sw.writeSysfs(file, value);
		else {
			sw = (SystemWriteManager)getActivity().getSystemService("system_write");
            sw.writeSysfs(file, value);
		}
	}

    void languageControlProcess(boolean value){
        	Log.d(TAG, "KEY_AUTO_LANGUAGE_SWITCH : " + value);
			SharedPreferences sharedpreference = getActivity()
					.getSharedPreferences(mAutoLanguagePreferencesFile,
							Context.MODE_PRIVATE);
			Editor editor = sharedpreference.edit();
			editor.putBoolean(KEY_AUTO_LANGUAGE_SWITCH, value);
			editor.commit();

			if (value) {
				cec_auto_language_switch.setSummary(R.string.on);
				if (!isMyServiceRunning()) {
					Intent serviceIntent = new Intent(getActivity(),
							CheckingService.class);
					serviceIntent.setAction("CEC_LANGUAGE_AUTO_SWITCH");
					getActivity().startService(serviceIntent);
				}
			} else {
				cec_auto_language_switch.setSummary(R.string.off);
			}


    }

	public boolean onPreferenceChange(Preference preference, Object objValue) {
		final String key = preference.getKey();
		Boolean value = (Boolean) objValue;
        Log.d(TAG, "==== value : " + value);
		if (KEY_AUTO_LANGUAGE_SWITCH.equals(key)) {

            languageControlProcess(value);

		} else if (KEY_EASYLINK.equals(key)) {
        
			Log.d(TAG, "KEY_EASYLINK : " + value);
			String cec_config = sw.getPropertyString(STR_CECCONFIG, "");
		    //Toast.makeText(getActivity(), "waitting for seconds", 4000).show();
			String configString = getBinaryString(cec_config);
				int tmpArray[] = getBinaryArray(configString);
				if (value) {

                    dialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.notice).setMessage(R.string.cec_loading).create();
                    dialog.show();
                    Handler h = new myHandle();
                    Message msg = new Message();
                    msg.what = 100 ;
                    h.sendMessageDelayed(msg, 4000);
                    Log.d(TAG, "=== show dialog()  ");
                    
					tmpArray[3] = 1;
					
					setAllEnable(true);
					setAllSummary(R.string.on);
					setAllCheck(true);
                    languageControlProcess(true);
                    sw.setProperty(STR_CECCONFIG, "cecf");
                    		
                    Thread t = new Thread(){
                        public void run() {
                             writeFile(writeCecConfig,"f");
                        };
                    };
                    t.start();
                   
                    Log.d(TAG,"====KEY_EASYLINK, set prop :" + STR_CECCONFIG + "= cecf");
				} else {
					tmpArray[3] = 0;
					
                    sw.setProperty(STR_CECCONFIG, "cec0");
                    Log.d(TAG,"====KEY_EASYLINK, set prop :" + STR_CECCONFIG + "= cec0");
					setAllCheck(false);
					setAllSummary(R.string.off);
					setAll(false);
                    languageControlProcess(false);
                    writeFile(writeCecConfig, "0");
				}

		} else if (KEY_ONE_TOUCH_PLAY.equals(key)) {
			Log.d(TAG, "KEY_ONE_TOUCH_PLAY : " + value);
            
			String cec_config = sw.getPropertyString(STR_CECCONFIG, "");
            String configString = getBinaryString(cec_config);
            int tmpArray[] = getBinaryArray(configString);
			if (cec_config.length() == 4) {
				if (value) {
					mOneTouchPlayPreference.setSummary(R.string.on);
                    tmpArray[2]=1;
                    tmpArray[0]=1;
                    //String str = setValueAtIndex(configString,2,'1');
					//writeFile(writeCecConfig, str);

				} else {
					mOneTouchPlayPreference.setSummary(R.string.off);
                    //String str = setValueAtIndex(configString,2,'0');
                    tmpArray[2]=0;
                    tmpArray[0]=1;
					//writeFile(writeCecConfig, str);

				}
				String writeConfig = arrayToString(tmpArray);
				sw.setProperty(STR_CECCONFIG, writeConfig);
                Log.d(TAG,"====KEY_ONE_TOUCH_PLAY, set prop :" + STR_CECCONFIG + "="+writeConfig);
                String s = writeConfig.substring(writeConfig.length() - 1,writeConfig.length());
                writeFile(writeCecConfig, s);
			} else {
				Log.d(TAG, "someting wrong with property : "
						+ STR_CECCONFIG);
			}

		} else if (KEY_ONE_TOUCH_STANDBY.equals(key)) {
			Log.d(TAG, "KEY_ONE_TOUCH_STANDBY : " + value);
			String cec_config = sw.getPropertyString(STR_CECCONFIG, "");
			if (cec_config.length() == 4) {
				String configString = getBinaryString(cec_config);
				int tmpArray[] = getBinaryArray(configString);
				if (value) {
					mOneTouchStandyByPreference.setSummary(R.string.on);
					tmpArray[1] = 1;
                    tmpArray[0]=1;
                    //String str = setValueAtIndex(configString,1,'1');
					//writeFile(writeCecConfig, str);

				} else {
					mOneTouchStandyByPreference.setSummary(R.string.off);
                    //String str = setValueAtIndex(configString,1,'0');
					tmpArray[1] = 0;
                    tmpArray[0]=1;
					//writeFile(writeCecConfig, str);

				}
				String writeConfig = arrayToString(tmpArray);
				sw.setProperty(STR_CECCONFIG, writeConfig);
                Log.d(TAG,"====KEY_ONE_TOUCH_STANDBY, set prop :" + STR_CECCONFIG + "="+writeConfig);
                String s = writeConfig.substring(writeConfig.length() - 1,writeConfig.length());
                writeFile(writeCecConfig, s);
			} else {
				Log.d(TAG, "someting wrong with property : "
						+ STR_CECCONFIG);
			}

		}

		return true;
	}

    	class myHandle extends Handler {

		@Override
		public void handleMessage(Message msg) {

			if (msg.what == 100) {
				dialog.dismiss();
                Log.d(TAG, "=== dismiss dialog()  ");
			}
		}

	}

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v instanceof ListView){
            if(hasFocus)
            {
                v.setOnKeyListener(mListOnKeyListener);
            }else{
                tipsView.setText(" ");
            }
        }
    }

    private OnKeyListener mListOnKeyListener = new OnKeyListener() {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if ( v  instanceof ListView){
                Object selectedItem = ((ListView)v).getSelectedItem();
                if ( selectedItem instanceof Preference){
                    if(((Preference)selectedItem).getKey().equals(KEY_EASYLINK)){
                        tipsView.setText(R.string.cec_link_tip);
                    }
                    if(((Preference)selectedItem).getKey().equals(KEY_ONE_TOUCH_PLAY)){
                        tipsView.setText(R.string.one_touch_play_tip);
                    }
                    if(((Preference)selectedItem).getKey().equals(KEY_ONE_TOUCH_STANDBY)){
                        tipsView.setText(R.string.one_touch_standby_tip);
                    }
                    if(((Preference)selectedItem).getKey().equals(KEY_AUTO_LANGUAGE_SWITCH)){
                        tipsView.setText(R.string.auto_switch_language_tip);
                    }
                }
            }
            return false;
        }
    };
}