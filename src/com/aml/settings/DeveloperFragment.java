package com.aml.settings;;

import java.io.File;

import android.app.SystemWriteManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;

public class DeveloperFragment extends PreferenceFragment implements
		Preference.OnPreferenceChangeListener {
	SystemWriteManager sw = null;
	private final static String TAG = "DeveloperFragment";
	private final static String DEVELOPER = "developer_sharepreference";
	private final static String DNLP = "developer_dnlp";
	WrappingSwitchPreference dnlp = null;
	String dnlp_sys_path = "/sys/class/aml_gamma_proc/gamma_proc";
	SharedPreferences developer_sharedpreference = null;

	@Override
	public boolean onPreferenceChange(Preference pref, Object value) {
		String key = pref.getKey();
		Boolean state = (Boolean) value;
		if (key.endsWith(DNLP)) {
			if (state) {
				
				dnlp.setSummary(R.string.on);
				sw.writeSysfs(dnlp_sys_path, "0x10200202");
				if (developer_sharedpreference != null) {
					Editor editor = developer_sharedpreference.edit();
					editor.putBoolean(DNLP, true);
					editor.commit();
					Log.d(TAG, "===== put true");
				}

			} else {
				
				sw.writeSysfs(dnlp_sys_path, "0x0");
				dnlp.setSummary(R.string.off);
				if (developer_sharedpreference != null) {
					Editor editor = developer_sharedpreference.edit();
					editor.putBoolean(DNLP, false);
					editor.commit();
					Log.d(TAG, "===== put false");
				}
			}

		}

		return true;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sw = (SystemWriteManager) getActivity()
				.getSystemService("system_write");

		addPreferencesFromResource(R.xml.developer_settings);
		dnlp = (WrappingSwitchPreference) findPreference(DNLP);
		dnlp.setOnPreferenceChangeListener(this);
		developer_sharedpreference = getActivity().getSharedPreferences(
				DEVELOPER, Context.MODE_PRIVATE);

		Boolean v = developer_sharedpreference.getBoolean(DNLP, false);
		Log.d(TAG, "===== developer_sharedpreference : " + v);
		if (v) {
			dnlp.setChecked(true);
			dnlp.setSummary(R.string.on);
		} else {
			dnlp.setChecked(false);
			dnlp.setSummary(R.string.off);
		}

		
		File sysfile = new File(dnlp_sys_path);
		if (sysfile.exists()) {
			dnlp.setEnabled(true);
			Log.d(TAG, "====path value = " + sw.readSysfs(dnlp_sys_path));
			if ("1".equals(sw.readSysfs(dnlp_sys_path))) {
				// dnlp.setChecked(true);
				// dnlp.setSummary("on");

			} else {
				// dnlp.setChecked(false);
				// dnlp.setSummary("off");
			}

		} else {

			dnlp.setEnabled(false);
		}

	}

	public static DeveloperFragment newInstance() {

		DeveloperFragment mInstance = new DeveloperFragment();
		Log.d(TAG, "===== newInstance()");

		return mInstance;
	}
}