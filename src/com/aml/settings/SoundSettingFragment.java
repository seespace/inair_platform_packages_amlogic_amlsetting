package com.aml.settings;

import android.app.SystemWriteManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.app.SystemWriteManager;

public class SoundSettingFragment extends PreferenceFragment implements
        Preference.OnPreferenceChangeListener  {
    
    private static final String TAG = "SoundSettingFragment";
    private static final String KEY_DIGIT_AUDIO_OUTPUT = "digit_audio_output";
    private static final String STR_DIGIT_AUDIO_OUTPUT = "ubootenv.var.digitaudiooutput";
    private static String DigitalRawFile = "/sys/class/audiodsp/digital_raw";
    private ListPreference mDigitAudioOutput = null ;
    private CharSequence[] mEntryValues;
    private int index_entry;
    private int sel_index;
    private SystemWriteManager sw = null;
    
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.sound_setting);
        sw = (SystemWriteManager) getActivity().getSystemService("system_write");
        initSound();
	}

	public static SoundSettingFragment newInstance() {
		SoundSettingFragment mInstance = new SoundSettingFragment();
		return mInstance;
	}
    
    private void initSound(){
         mDigitAudioOutput = (ListPreference) findPreference(KEY_DIGIT_AUDIO_OUTPUT);
        if(platformHasDigitAudio()){
           
            mDigitAudioOutput.setOnPreferenceChangeListener(this);
            mDigitAudioOutput.setEnabled(true);

            String valDigitAudioOutput = sw.getProperty(STR_DIGIT_AUDIO_OUTPUT);
            mEntryValues = getResources().getStringArray(R.array.digit_audio_output_entries);
            index_entry = findIndexOfEntry(valDigitAudioOutput, mEntryValues);
            mDigitAudioOutput.setValueIndex(index_entry);
            if(sel_index == 0){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"PCM");
                sw.writeSysfs(DigitalRawFile, "0");
                mDigitAudioOutput.setSummary("PCM");
                Log.i(TAG,"digit audio output set to PCM");
            }else if(sel_index == 1){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"RAW");
                sw.writeSysfs(DigitalRawFile, "1");
                mDigitAudioOutput.setSummary("RAW");
                Log.i(TAG,"digit audio output set to RAW");
            }else if(sel_index == 2){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"SPDIF passthrough");
                sw.writeSysfs(DigitalRawFile, "1");
                mDigitAudioOutput.setSummary("SPDIF passthrough");
                Log.i(TAG,"digit audio output set to SPDIF passthrough");
            }else if(sel_index == 3){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"HDMI passthrough");
                sw.writeSysfs(DigitalRawFile, "2");
                mDigitAudioOutput.setSummary("HDMI passthrough");
                Log.i(TAG,"digit audio output set to HDMI passthrough");
            }
            
            if(!valDigitAudioOutput.equals(""))
                mDigitAudioOutput.setSummary(valDigitAudioOutput);
        } else{
            mDigitAudioOutput.setEnabled(false);
        }
    }



    public boolean onPreferenceChange(Preference preference, Object objValue) {
        final String key = preference.getKey();
        if(KEY_DIGIT_AUDIO_OUTPUT.equals(key)){
            sel_index = Integer.parseInt((String) objValue);
            if(sel_index == 0){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"PCM");
                sw.writeSysfs(DigitalRawFile, "0");
                mDigitAudioOutput.setSummary("PCM");    
                Log.i(TAG,"digit audio output set to PCM");
            }
            else if(sel_index == 1){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"RAW");
                sw.writeSysfs(DigitalRawFile, "1");
                mDigitAudioOutput.setSummary("RAW");    
                Log.i(TAG,"digit audio output set to RAW");
            }
            else if(sel_index == 2){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"SPDIF passthrough");
                sw.writeSysfs(DigitalRawFile, "1");
                mDigitAudioOutput.setSummary("SPDIF passthrough");    
                Log.i(TAG,"digit audio output set to SPDIF passthrough");
            }
            else if(sel_index == 3){
                sw.setProperty(STR_DIGIT_AUDIO_OUTPUT,"HDMI passthrough");
                sw.writeSysfs(DigitalRawFile, "2");
                mDigitAudioOutput.setSummary("HDMI passthrough");  
                Log.i(TAG,"digit audio output set to HDMI passthrough");
            }
        }
        return true;
    }

    private int findIndexOfEntry(String value, CharSequence[] entry) {
        if (value != null && entry != null) {
            for (int i = entry.length - 1; i >= 0; i--) {
                if (entry[i].equals(value)) {
                    return i;
                }
            }
        }
        return 0;  //set PCM as default
    }

    private boolean platformHasDigitAudio() {
        return sw.getPropertyBoolean("ro.platform.has.digitaudio", false);
    } 
    
}
