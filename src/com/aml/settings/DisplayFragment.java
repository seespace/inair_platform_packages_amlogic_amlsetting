package com.aml.settings;

import android.app.SystemWriteManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;

public class DisplayFragment extends PreferenceFragment implements
		Preference.OnPreferenceChangeListener {
	private static final String TAG = "DisplayFragment";
	CheckBoxPreference mRequstRoateScreen = null;
	private final String ACTION_OUTPUTMODE_SAVE = "android.intent.action.OUTPUTMODE_SAVE";
    private final String ACTION_CVBSMODE_CHANGE = "android.intent.action.CVBSMODE_CHANGE";
	private SystemWriteManager sw = null;
    
	private DisplaySettingsListPreference mOutputmode;
	private static final String KEY_REQUEST_ROTATE = "requestrotate";
	private static final String KEY_ACCELEROMETER = "accelerometer";
	private static final String KEY_AUTO_OUTPUT_MODE = "auto_output_mode";
	private final String OUTPUT_MODE = "output_mode";
	private static final String KEY_OUTPUTMODE = "output_mode";
	private static final String KEY_CVBSMODE = "dual_output_mode";
    private static final String KEY_Default_CVBSMODE = "cvbs_output_mode";
	private static final String KEY_FONT_SIZE = "font_size";
	private static final String KEY_NOTIFICATION_PULSE = "notification_pulse";
	private static final String KEY_Brightness = "brightness";
	private static final String KEY_DISPLAY_POSITION = "display_position";
	private static final String KEY_DEFAULT_FREQUENCY = "default_frequency";
	private static final String KEY_SCREEN_SAVER = "screensaver";
    private static final String KEY_KEEP_LANDSCAPE_IN_LAUNCHER = "keeplandscape";
    private static final String STR_KEEP_LANDSCAPE_IN_LAUNCHER = "keep.landscape.in.launcher2";
	// private static final String STR_OUTPUT_MODE = "ubootenv.var.outputmode";
	private static final String STR_HAS_CVBS = "ro.platform.has.cvbsmode";
    private static final String STR_HDMI_ONLY = "ro.platform.hdmionly";
	private static final String STR_CVBS_MODE = "ubootenv.var.cvbsmode";
	private static final String PROPERTY_AUTO_OUTPUT_MODE = "auto.output.mode.property";
	private static final String ACTION_AUTO_OUTPUT_MODE = "android.intent.action.AUTO_OUTPUT_MODE";
	private static final String BOOLEAN_AUTO_OUTPUT_MODE = "auto_output_mode";
	private static final String PREFERENCE_AUTO_OUTPUT_MODE = "preference_auto_output_mode";
	private static final String STATUS_BAR_CONTROL = "status_bar_control";
    private static final String PREFERENCE_KEEP_LANDSCAPE = "preference_keep_landscape_in_launcher";
    
	private CharSequence[] mEntryValues;
	private int sel_index;
	private int index_entry;
	private CheckBoxPreference mAutoOutputMode;
	private HDMICheckHandler mHDMIHandler = null;
	private Preference mDisplayposition;
    private ListPreference  mDualCvbsOutput;
	private ListPreference  mDisplayOutputmode;
	private CheckBoxPreference mStatusBarControl;
	private static final int GET_USER_OPERATION=1;
	private int index_cvbs;
    private int select_cvbs;
    private CheckBoxPreference mKeepLandscapeInLauncher ;
    private ListPreference  mDefaultCvbsMode;
    public static Handler handler = null;
    public static boolean isDongle = false ;

	@Override
	public void onDestroy() {
		if (mHDMIHandler != null)
			mHDMIHandler.stopHDMICheck();
			super.onDestroy();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Config.Instance(getActivity());
		sw = (SystemWriteManager) getActivity().getSystemService("system_write");

		mHDMIHandler = new HDMICheckHandler(getActivity(), true);
		mHDMIHandler.startHDMICheck();
        
		addPreferencesFromResource(R.xml.display_settings);
        initMode();
        handler = new Handler(){
            @Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
                if (msg.arg1==HDMICheckHandler.DISABLE_OUTOUTMODE_SETTING) {
                    if(mOutputmode!=null){
                        if(mOutputmode.getDialog()!=null){
                            mOutputmode.getDialog().dismiss();
                        }
                        mOutputmode.setEnabled(false);
                        mOutputmode.setSelectable(false);
                    }
                    if(mDefaultCvbsMode != null) {
                        mDefaultCvbsMode.setEnabled(true);
                        mDefaultCvbsMode.setSelectable(true);
                    }
                }
                if(msg.arg1==HDMICheckHandler.ENABLE_OUTOUTMODE_SETTING){
                    if(mAutoOutputMode!=null && !mAutoOutputMode.isChecked()){
                        if(mOutputmode!=null){
                            mOutputmode.setEnabled(true);
                            mOutputmode.setSelectable(true);
                        }
                    }
                    if(mDefaultCvbsMode != null) {
                        if(mDefaultCvbsMode.getDialog()!=null){
                            mDefaultCvbsMode.getDialog().dismiss();
                        }
                        mDefaultCvbsMode.setEnabled(false);
                        mDefaultCvbsMode.setSelectable(false);
                    }
                }
                super.handleMessage(msg);
            }
        };
        
	}

    private void initMode(){      
        mOutputmode = (DisplaySettingsListPreference) findPreference(KEY_OUTPUTMODE);
        isDongle = "true".equals(getResources().getString(R.bool.isDongle));
        if(isDongle){
            String[] outmode_dongle_entries = getResources().getStringArray(R.array.outputmode_entries_display_dongle);
            mOutputmode.setEntries(outmode_dongle_entries);
        }
        mOutputmode.setOnPreferenceChangeListener(this);
        mRequstRoateScreen = (CheckBoxPreference) findPreference(KEY_REQUEST_ROTATE);
		mRequstRoateScreen.setOnPreferenceChangeListener(this);
        mKeepLandscapeInLauncher = (CheckBoxPreference) findPreference(KEY_KEEP_LANDSCAPE_IN_LAUNCHER);
        mKeepLandscapeInLauncher.setOnPreferenceChangeListener(this);
        updateRequestRotationCheckbox();
        mAutoOutputMode = (CheckBoxPreference)findPreference(KEY_AUTO_OUTPUT_MODE);
        mAutoOutputMode.setOnPreferenceChangeListener(this);
        SharedPreferences sharedpreference = getActivity().getSharedPreferences(
            PREFERENCE_AUTO_OUTPUT_MODE, Context.MODE_PRIVATE);

        String defaultProperty = null;
        if(HDMICheckHandler.mAutoStartConfig){
            defaultProperty = sharedpreference.getString(PROPERTY_AUTO_OUTPUT_MODE, "true");
        }else{
            defaultProperty = sharedpreference.getString(PROPERTY_AUTO_OUTPUT_MODE, "false");
        }
        
        Editor editor = sharedpreference.edit();
        editor.putString(PROPERTY_AUTO_OUTPUT_MODE, defaultProperty);
        editor.commit();
        
        if(defaultProperty.equalsIgnoreCase("true")){
            mAutoOutputMode.setChecked(true);
            mOutputmode.setEnabled(false);
            if(HDMICheckHandler.isHDMIPlugged(sw)){
                Log.d(TAG,"HDMI was Plugged");
			    mOutputmode.setSelectable(false);
            }
                
        }else{
            mAutoOutputMode.setChecked(false);
            mOutputmode.setEnabled(true);
			mOutputmode.setSelectable(true);
        }

        String valOutputmode = Config.getCurrentOutputResolution(sw);
        if(isDongle){
            mEntryValues = getResources().getStringArray(R.array.outputmode_entries_display_dongle);            
        }else{
            mEntryValues = getResources().getStringArray(R.array.outputmode_entries_display);
        }
        index_entry = findIndexOfEntry(Config.Logic2DisplayEntry(valOutputmode), mEntryValues);
        mOutputmode.setValueIndex(index_entry);
        Config.Logd(getClass().getName(), "index_entry is: " + index_entry);

        if(mAutoOutputMode.isChecked()){
            if(HDMICheckHandler.isHDMIPlugged(sw)){
                mOutputmode.setEnabled(false);
                mOutputmode.setSelectable(false);
            }
        }else{
            mOutputmode.setEnabled(true);
            mOutputmode.setSelectable(true);
        }

        
        if(HDMICheckHandler.isHDMIPlugged(sw)){
           if(mAutoOutputMode.isChecked()){
                mOutputmode.setEnabled(false);
                mOutputmode.setSelectable(false);
           }

        }else{
                mOutputmode.setEnabled(true);
                mOutputmode.setSelectable(true);
        }

        //Dual output mode 
        if (sw.getPropertyBoolean(STR_HAS_CVBS,false)) {
            mDualCvbsOutput = (ListPreference) findPreference(KEY_CVBSMODE);
            mDualCvbsOutput.setOnPreferenceChangeListener(this);
            String cvbs_output_mode = sw.getProperty(STR_CVBS_MODE);
            String outputmode = sw.getProperty("ubootenv.var.outputmode");     
            if(outputmode.equals("576i")||outputmode.equals("480i"))
                mDualCvbsOutput.setEnabled(false);
            else
                mDualCvbsOutput.setEnabled(true);
            if(cvbs_output_mode.equals("480cvbs")){
                mDualCvbsOutput.setValueIndex(0);
                index_cvbs = 0;
            }else if(cvbs_output_mode.equals("576cvbs")){
                mDualCvbsOutput.setValueIndex(1);
                index_cvbs = 1;
            }else{
                mDualCvbsOutput.setEnabled(false);
            }        
        }else{
            getPreferenceScreen().removePreference(findPreference(KEY_CVBSMODE));
        }
        // CVBS mode,this case is used in hdmi only mode to set default cvbs outputmode,
        if (sw.getPropertyBoolean(STR_HDMI_ONLY,false)) {
            mDefaultCvbsMode= (ListPreference) findPreference(KEY_Default_CVBSMODE);
            mDefaultCvbsMode.setOnPreferenceChangeListener(this);
            String cvbs_output_mode = sw.getPropertyString(STR_CVBS_MODE,"480cvbs");
            if(cvbs_output_mode.equals("480cvbs")){
                mDefaultCvbsMode.setValueIndex(0);
                index_cvbs = 0;
            }else if(cvbs_output_mode.equals("576cvbs")){
                mDefaultCvbsMode.setValueIndex(1);
                index_cvbs = 1;
            }
            if(!HDMICheckHandler.isHDMIPlugged(sw)){
                mOutputmode.setEnabled(false);
                mOutputmode.setSelectable(false);
                
                mDefaultCvbsMode.setEnabled(true);
                mDefaultCvbsMode.setSelectable(true);
            } else {
                mDefaultCvbsMode.setEnabled(false);
                mDefaultCvbsMode.setSelectable(false);
            }
        }else{
            getPreferenceScreen().removePreference(findPreference(KEY_Default_CVBSMODE));
        }
        
        mDisplayposition = findPreference(KEY_DISPLAY_POSITION);
        mDisplayposition.setOnPreferenceChangeListener(this);
        mDisplayposition.setPersistent(false);

        mStatusBarControl = (CheckBoxPreference)findPreference(STATUS_BAR_CONTROL);
        mStatusBarControl.setOnPreferenceChangeListener(this);
        mStatusBarControl.setPersistent(true);
        hwSetStatusBar(String.valueOf(mStatusBarControl.isChecked()));
    }
    

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		String key = preference.getKey();
	    Log.d(TAG, "onPreferenceTreeClick() , key = " + key);
		if (preference == mDisplayposition) {
			Intent intent = new Intent();
			Bundle bundle = new Bundle();
			this.setMenuVisibility(false);
			// intent.setComponent(new
			// ComponentName("com.android.settings","com.aml.settings.PositionSetting"));
			intent.setClass(getActivity(), PositionSetting.class);
			intent.putExtras(bundle);
			startActivity(intent);
		}else if (KEY_OUTPUTMODE.equals(key)) {
             Log.d(TAG, "KEY_OUTPUTMODE");
            // get current mode used in uboot
            String currentMode = Config.getCurrentOutputResolution(sw);
            Log.d(TAG, "currentMode is: " + currentMode);
            if (currentMode != null) {
                int index = -1;
                CharSequence mode_entries[] = mOutputmode.getEntries();
                for (index = 0; index < mode_entries.length; index++) {
                    String mode_string = ""+ Config.Display2LogicEntry(mode_entries[index]+ "");
                    Log.d(TAG, "mode_string is: " + mode_string);
                    if (currentMode.equalsIgnoreCase(mode_string)) {
                        break;
                    }
                }
                Log.d(TAG,"mode_entries.length is: " + mode_entries.length);
                Log.d(TAG, "index is: " + index);
                if (index < mode_entries.length) {
                    Log.d(TAG, "mode_entry is: "+ mode_entries[index]);
                    index_entry = sel_index = index;
                    mOutputmode.setValueIndex(index);
                }
            }
        }
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}
    
	private int findIndexOfEntry(String value, CharSequence[] entry) {
		if (value != null && entry != null) {
			for (int i = entry.length - 1; i >= 0; i--) {
				if (entry[i].equals(value)) {
					return i;
				}
			}
		}
		if(DisplayFragment.isDongle){
		    return getResources().getInteger(R.integer.outputmode_default_values_dongle);
        }
		return getResources().getInteger(R.integer.outputmode_default_values);
	}

	@Override
	public boolean onPreferenceChange(Preference pref, Object value) {
		
		boolean check = "true".equals(value.toString());
		String key = pref.getKey();
        Log.d(TAG, "onPreferenceChange() , key = " + key + ", value=" +value.toString());
        
		if (KEY_REQUEST_ROTATE.equals(key)) {  
            Log.d(TAG, "onPreferenceChange() ,KEY_REQUEST_ROTATE");
			hwSetAccelerometer(value.toString());
			mRequstRoateScreen.setChecked(check);

            mKeepLandscapeInLauncher.setEnabled(check);
            mKeepLandscapeInLauncher.setSelectable(check);
        } else if(KEY_KEEP_LANDSCAPE_IN_LAUNCHER.equals(key)) {
            mKeepLandscapeInLauncher.setChecked(check);
            setKeepLauncherLandscape(value.toString());
        }else if (KEY_AUTO_OUTPUT_MODE.equals(key)) {
            Log.d(TAG, "onPreferenceChange() ,KEY_AUTO_OUTPUT_MODE");
            mAutoOutputMode.setChecked(check);
            Editor sharedpreferenceEditor = getActivity().getSharedPreferences(
						PREFERENCE_AUTO_OUTPUT_MODE,Context.MODE_PRIVATE).edit();
            sharedpreferenceEditor.putString(PROPERTY_AUTO_OUTPUT_MODE, value.toString());
            sharedpreferenceEditor.commit();
            if( ! HDMICheckHandler.isHDMIPlugged(sw)){
                if(sw.getPropertyBoolean(STR_HDMI_ONLY,false)){
                    mOutputmode.setEnabled(false);
                    mOutputmode.setSelectable(false);
                }else{
                    mOutputmode.setEnabled(true);
                    mOutputmode.setSelectable(true);
                }
                
            }else{
                if(check){
                    mOutputmode.setEnabled(false);
					mOutputmode.setSelectable(false);
                }else{
                    mOutputmode.setEnabled(true);
				    mOutputmode.setSelectable(true);
                }
            }
			Intent auto_output_mode_intent = new Intent(ACTION_AUTO_OUTPUT_MODE);
			SharedPreferences sharedpreference = getActivity().getSharedPreferences(PREFERENCE_AUTO_OUTPUT_MODE,Context.MODE_PRIVATE);
			String auto_output_mode_property = sharedpreference.getString(PROPERTY_AUTO_OUTPUT_MODE, "false");
			auto_output_mode_intent.putExtra(BOOLEAN_AUTO_OUTPUT_MODE,auto_output_mode_property);

			if (auto_output_mode_property != null) {
				if (auto_output_mode_property.equalsIgnoreCase("true")
						|| auto_output_mode_property.equalsIgnoreCase("false")) {
					getActivity().sendBroadcast(auto_output_mode_intent);
				}
			}
		}else if(KEY_OUTPUTMODE.equals(key)){
            try {
                sel_index = Integer.parseInt((String) value);
                Config.Logd(getClass().getName(), "parseInt(), sel_index is: " + sel_index);
                Config.Logd(getClass().getName(), "index_entry is: " + index_entry);
                if(index_entry!=sel_index){
                    Intent intent = new Intent(getActivity(),OutputSetConfirm.class);
                    intent.putExtra("set_mode", Config.Display2LogicEntry(mEntryValues[sel_index] + ""));
                    Config.Logd(getClass().getName(), Config.Display2LogicEntry(mEntryValues[sel_index] + ""));
                    startActivityForResult(intent, GET_USER_OPERATION);
                }
            }catch (NumberFormatException e){
                Log.e(TAG, "could not persist output mode setting", e);
            }
        } else  if(KEY_CVBSMODE.equals(key)){
            try {
                select_cvbs = Integer.parseInt((String) value);
                if(index_cvbs!=select_cvbs){
                    Intent intent_cvbsmode_change = new Intent(ACTION_CVBSMODE_CHANGE);
                    String cvbsMode=null;
                    if(select_cvbs==0){
                        cvbsMode = "480cvbs";
                    }
                    if(select_cvbs==1){
                        cvbsMode = "576cvbs";
                    }
                    sw.setProperty(STR_CVBS_MODE,cvbsMode);
                    index_cvbs = select_cvbs;
                    intent_cvbsmode_change.putExtra("cvbs_mode", cvbsMode);
                    getActivity().sendBroadcast(intent_cvbsmode_change);
                }
                 return true;
            }
            catch (NumberFormatException e) {
                Log.e(TAG, "could not persist output mode setting", e);
            } 
        }else if (STATUS_BAR_CONTROL.equals(key)){
            mStatusBarControl.setChecked(check);
            hwSetStatusBar(value.toString());
            //getActivity().recreate();
        }else if(KEY_Default_CVBSMODE.equals(key)){
           try {
                select_cvbs = Integer.parseInt((String) value);
                if(index_cvbs!=select_cvbs){
                    String cvbsMode=null;
                    if(select_cvbs==0){
                        cvbsMode = "480cvbs";
                    }
                    if(select_cvbs==1){
                        cvbsMode = "576cvbs";
                    }
                    sw.setProperty(STR_CVBS_MODE,cvbsMode);

                    Intent intent = new Intent(getActivity(),OutputSetConfirm.class);
                    intent.putExtra("set_mode", cvbsMode);
                    Config.Logd(getClass().getName(), cvbsMode);
                    startActivityForResult(intent, GET_USER_OPERATION);
                    
                    index_cvbs = select_cvbs;
                }
            }
            catch (NumberFormatException e) {
                Log.e(TAG, "could not persist output mode setting", e);
            }   
      }
        return true;
	}

	private void updateRequestRotationCheckbox() {
		if (mRequstRoateScreen != null){            
			mRequstRoateScreen.setChecked(hwHasAccelerometer());
            mKeepLandscapeInLauncher.setChecked(isKeepLauncherLandscape());
            mKeepLandscapeInLauncher.setEnabled(hwHasAccelerometer());
            mKeepLandscapeInLauncher.setSelectable(hwHasAccelerometer());
            }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	public static DisplayFragment newInstance() {
		DisplayFragment mInstance = new DisplayFragment();
		return mInstance;
	}

    
    private void hwSetStatusBar(String s) {
        sw.setProperty("persist.sys.hideStatusBar", s);
    }
    
    private boolean hwGetStatusBar() {
        return sw.getPropertyBoolean("persist.sys.hideStatusBar", true);
    }

	private boolean hwHasAccelerometer() {
		return sw.getPropertyBoolean("ubootenv.var.has.accelerometer", true);
	}

	private void hwSetAccelerometer(String b) {
		sw.setProperty("ubootenv.var.has.accelerometer", b);
	}

	private boolean isKeepLauncherLandscape() {
		return sw.getPropertyBoolean("sys.keeplauncher.landcape", false);
	}

	private void setKeepLauncherLandscape(String b) {
		sw.setProperty("sys.keeplauncher.landcape", b);
        
        Editor sharedpreferenceEditor = getActivity().getSharedPreferences(
					PREFERENCE_KEEP_LANDSCAPE,Context.MODE_PRIVATE).edit();
        sharedpreferenceEditor.putString(STR_KEEP_LANDSCAPE_IN_LAUNCHER, b);
        sharedpreferenceEditor.commit();
	}

	private boolean platformHasTvOutput() {
		return sw.getPropertyBoolean("ro.screen.has.tvout", false);
	}

    	@Override
	public  void onActivityResult(int requestCode,int resultCode,Intent data)
	{
		super.onActivityResult(requestCode,resultCode,data);
        Log.d(TAG,"requestCode:"+requestCode + ", resultCode="+resultCode);
        switch(requestCode){
            case (GET_USER_OPERATION):
                if(resultCode==Activity.RESULT_OK){
                    String []values = null;
                    if(isDongle){
                        values = getResources().getStringArray(R.array.outputmode_entries_display_dongle);
                    }else{
                        values = getResources().getStringArray(R.array.outputmode_entries_display);
                    }
                    String tv_outputmode = Config.Display2LogicEntry(values[sel_index]);
                    Config.Logd(getClass().getName(), "tv_outputmode is: " + tv_outputmode);                   
                    Intent intent_outputmode_save = new Intent(ACTION_OUTPUTMODE_SAVE);
                    intent_outputmode_save.putExtra(OUTPUT_MODE, tv_outputmode);
                    getActivity().sendBroadcast(intent_outputmode_save);       
                    index_entry = sel_index;
                    mOutputmode.setValueIndex(index_entry);
                    if (sw.getPropertyBoolean(STR_HAS_CVBS,false)) {
                        if(tv_outputmode.equals("576i")||tv_outputmode.equals("480i"))
                            mDualCvbsOutput.setEnabled(false);
                        else
                            mDualCvbsOutput.setEnabled(true);
                    }
                }
            else if(resultCode==Activity.RESULT_CANCELED){
                mOutputmode.setValueIndex(index_entry);
            }
        }
	}

}
