package com.aml.settings;

import java.util.List;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.app.Activity;
import android.content.Intent;
import android.os.SystemProperties;
import android.content.Context;


public class PreferenceWithHeaders extends PreferenceActivity {

	private final String TAG = "PreferenceWithHeaders";
    private static final int GET_USER_OPERATION=1;
    private int sel_index;
	private int index_entry;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/**
	 * Populate the activity with the top-level headers.
	 */
	@Override
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.heards, target);
        updateHeaderList(target);       
	}

    private void updateHeaderList(List<Header> target) {
        int i = 0;
        while (i < target.size()){
            Header header = target.get(i);
            int id = (int) header.id;
            if(isMbxUI()){
                if(id == R.id.developer_settings ){
                    target.remove(i);
                }
            }else{
                if(id == R.id.display_settings ){
                    target.remove(i);
                }else if(id == R.id.cec_setting){       
                    target.remove(i);
                }else if(id == R.id.sound_settings){       
                    target.remove(i);
                }else if(id == R.id.network_status){
                    target.remove(i);
                }
            }
            i++;
       } 
    }

    public static boolean isMbxUI(){
        return SystemProperties.getBoolean("ro.platform.has.mbxuimode", false);
    }

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
  public void onHeaderClick(Header header, int position) {
    if (header.id == R.id.cec_setting) {
        MyPreferenceFragment cecFragment = MyPreferenceFragment.newInstance();
    } else if (header.id == R.id.display_settings) {
        DisplayFragment newFragment =  DisplayFragment.newInstance();
    }else if(header.id == R.id.sound_settings){
        SoundSettingFragment newFragment = SoundSettingFragment.newInstance();
    }else if(header.id == R.id.developer_settings){
        DeveloperFragment newFragment = DeveloperFragment.newInstance();
    }else if (header.id == R.id.network_status) {
        NetworkStatus newFragment =new NetworkStatus();
    }
		super.onHeaderClick(header, position);
	}


	@Override
	public  void onActivityResult(int requestCode,int resultCode,Intent data)
	{
		super.onActivityResult(requestCode,resultCode,data);
        Log.d(TAG,"===== onActivityResult()");
    }
}
