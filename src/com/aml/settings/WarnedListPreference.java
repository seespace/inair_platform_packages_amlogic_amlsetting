package com.aml.settings;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class WarnedListPreference extends ListPreference {
	public WarnedListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onClick() {
		// Ignore this until an explicit call to click()
	}

	public void click() {
		super.onClick();
	}
}
