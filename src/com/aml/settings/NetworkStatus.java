
package com.aml.settings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.app.Activity;
import android.app.Fragment;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObserver;

import android.net.wifi.WifiManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.os.Bundle;
import android.os.Environment;
import android.net.ethernet.EthernetDevInfo;
import android.net.ethernet.EthernetManager;
import android.net.LinkProperties;
import android.net.ethernet.EthernetStateTracker;
import android.net.NetworkConfig;




import android.text.StaticLayout;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;


import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.ListView;
import android.os.SystemProperties;
import android.app.SystemWriteManager;



//import com.android.internal.content.PackageHelper;

//import com.android.settings.R;
//import com.android.settings.Utils;

public class NetworkStatus extends Fragment implements
        TabHost.TabContentFactory, TabHost.OnTabChangeListener {
    static final String TAG = "NetworkStatus";
    static final boolean DEBUG = false;
    
    // attributes used as keys when passing values to InstalledAppDetails activity
    public static final String MAC_ADDRESS = "/sys/devices/virtual/net/eth0/address";
	public static final String ETHERNET_DEV_STATUS = "/sys/devices/virtual/net/eth0/operstate";

    // layout inflater object used to inflate views
    private LayoutInflater mInflater;
    private SystemWriteManager sw = null;


    private TabHost mTabHost;
	private View mRootView;
    private String mDefaultTab = null;
	private View mListContainer;
	private View mListAdapter;
    private View mLoadingContainer;
	private ListView mListView;
	private TextView mStatusName;
	private TextView mStatusInfo;
	
    static final String TAB_WIFI = "WIFI";
    static final String TAB_ETHNET = "IPV4";
    static final String TAB_PPOE = "PPPOE";
    static final String TAB_IPV6 = "IPV6";
    String[] netstatusname;
    String[] netstatusinfo;
    
    static class TextViewHoder{
    	TextView statusname;
    	TextView statusinfo;    	
    	
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sw = (SystemWriteManager) getActivity().getSystemService("system_write");
              
    }
	   @Override
	   	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	   	{
	   	// Inflate the layout for this fragment
	   	mRootView = inflater.inflate(R.layout.network_status, container, false);
		
		//mListView = (ListView) mRootView.findViewById(R.id.netstatus_list);
	    //mLoadingContainer = mRootView.findViewById(R.id.loading_container);
        mListContainer = mRootView.findViewById(R.id.list_view);
        ListView lv = (ListView) mListContainer.findViewById(R.id.listview);
        Resources res =getResources();
        netstatusname = res.getStringArray(R.array.network_status_entries);
        mListView =lv;
        // this tmpinfo array should be wifi status info,now have no idea to get wifi info,so set null at present
        String ip_address=null;
	    String subnet_mask=null;
	    String gate_way=null;
	    String dns=null;
	    String mac_address=null;
	    mac_address = getWifiMacAddress();
        String [] tmpinfo ;//= {"","","","",mac_address}; 
        if(sw.getPropertyString("init.svc.dhcpcd_wlan0",null).equals("running")){
        	ip_address = sw.getPropertyString("dhcp.wlan0.ipaddress",null);
        	subnet_mask = sw.getPropertyString("dhcp.wlan0.mask",null);
        	gate_way =  sw.getPropertyString("dhcp.wlan0.gateway",null);
        	dns =  sw.getPropertyString("dhcp.wlan0.dns1",null);
        	tmpinfo  = new String[] {ip_address,subnet_mask,gate_way,dns,mac_address};
        }else {
        	tmpinfo  = new String[] {null,null,null,null,mac_address};
		}
        NetStatusAdapter netStatusAdapter = new NetStatusAdapter(getActivity(), netstatusname, tmpinfo);
        mListView.setAdapter(netStatusAdapter);
		mTabHost = (TabHost) inflater.inflate(R.layout.network_status_tabhost, container, false);
	        mTabHost.setup();
	        //final TabHost tabHost = mTabHost;
	        mTabHost.addTab(mTabHost.newTabSpec(TAB_WIFI)
	                .setIndicator(getActivity().getString(R.string.wifistatus))
	                .setContent(this));
	        mTabHost.addTab(mTabHost.newTabSpec(TAB_ETHNET)
	                    .setIndicator(getActivity().getString(R.string.ethnet_status))
	                    .setContent(this));

	       /* mTabHost.addTab(mTabHost.newTabSpec(TAB_PPOE)
	                .setIndicator(getActivity().getString(R.string.ppoe_status))
	                .setContent(this));*/
	        mTabHost.addTab(mTabHost.newTabSpec(TAB_IPV6)
	                .setIndicator(getActivity().getString(R.string.ipv6_status))
	                .setContent(this));
	        mTabHost.setCurrentTabByTag(TAB_WIFI);
	        mTabHost.setOnTabChangedListener(this);
			//if the numbers of tab is four, use this marked codes to resolve last tab cannot move 
			//View mView = mTabHost.getTabWidget().getChildTabViewAt(2);
			View mView = mTabHost.getTabWidget().getChildTabViewAt(1);
			if(mView != null) mView.setId(1);
			//View mView1 = mTabHost.getTabWidget().getChildTabViewAt(3);
			View mView1 = mTabHost.getTabWidget().getChildTabViewAt(2);
			if(mView1 != null)  mView1.setNextFocusLeftId(1);

	        return mTabHost;
		}


   
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
       
    }

    @Override
    public void onPause() {
        super.onPause();
        
    }


	@Override
	public void onTabChanged(String tabId) {
		 try {
			    String []statusinfo = showCurrentTab();
			 	if (TAB_IPV6.equalsIgnoreCase(tabId)) {
			 		String [] ipv6Netinfo= getResources().getStringArray(R.array.ipv6_network_status_entries);
			 		NetStatusAdapter netStatusAdapter = new NetStatusAdapter(getActivity(), ipv6Netinfo, statusinfo);
		        	mListView.setAdapter(netStatusAdapter);
				}else {
					
		        	NetStatusAdapter netStatusAdapter = new NetStatusAdapter(getActivity(), netstatusname, statusinfo);
		        	mListView.setAdapter(netStatusAdapter);
				}
	        	
			} catch (Exception e) {
				e.printStackTrace();
			}
		
	}
	
	   public String [] showCurrentTab() {
		   
		    String ip_address=null;
		    String subnet_mask=null;
		    String gate_way=null;
		    String dns=null;
		    String mac_address=null;
	        String tabId = mTabHost.getCurrentTabTag();
			String ethernet_dev_status = getEthernetDevStatus();
	        String statusinfo[];
			EthernetManager mEthManager;
			mEthManager = (EthernetManager) getActivity().getSystemService(Context.ETH_SERVICE);
			EthernetDevInfo info = mEthManager.getSavedEthConfig();
			//LinkProperties mLinkProperties = new LinkProperties();
			//NetworkConfig[] mNetConfigs;
			//mNetConfigs = new NetworkConfig[ConnectivityManager.MAX_NETWORK_TYPE+1];
			//int netType = ConnectivityManager.TYPE_ETHERNET;
			//EthernetStateTracker mEthernetStateTrackere = new EthernetStateTracker(netType,mNetConfigs[netType].name);
			//LinkProperties mLinkProperties = mEthernetStateTrackere.getV6LinkProperties();
			
	        statusinfo  = new String[] {null,null,null,null,null};
	        if (TAB_WIFI.equalsIgnoreCase(tabId)) {
	        	mac_address = getWifiMacAddress();
	            if(sw.getPropertyString("init.svc.dhcpcd_wlan0",null).equals("running")){
	            	ip_address = sw.getPropertyString("dhcp.wlan0.ipaddress",null);
	            	subnet_mask = sw.getPropertyString("dhcp.wlan0.mask",null);
	            	gate_way =  sw.getPropertyString("dhcp.wlan0.gateway",null);
	            	dns =  sw.getPropertyString("dhcp.wlan0.dns1",null);
	            	statusinfo  = new String[] {ip_address,subnet_mask,gate_way,dns,mac_address};
	            }else {
	            	statusinfo  = new String[] {null,null,null,null,mac_address};
	    		}
	            
	        } else if (TAB_ETHNET.equalsIgnoreCase(tabId)) {
	        	mac_address = readMacAddress();
	        	//init.svc.dhcpcd_eth0 always equals running when ehernet is closed ,so use init.svc.dhcpcd_wlan0 too
	        	//if(SystemProperties.get("init.svc.dhcpcd_eth0",null).equals("running")&&
	        			//(!SystemProperties.get("init.svc.dhcpcd_wlan0",null).equals("running"))){		
				if(ethernet_dev_status.equals("up")&&
						info.getConnectMode().equals(EthernetDevInfo.ETH_CONN_MODE_DHCP)){
					Log.d(TAG,"----------connected mode:dhcp");
					if (isIpV4Address(sw.getPropertyString("dhcp.eth0.ipaddress",null))) {
					ip_address = sw.getPropertyString("dhcp.eth0.ipaddress",null);
	            	subnet_mask = sw.getPropertyString("dhcp.eth0.mask",null);
	            	gate_way =  sw.getPropertyString("dhcp.eth0.gateway",null);
	            	dns =  sw.getPropertyString("dhcp.eth0.dns1",null);
	            	statusinfo  = new String[] {ip_address,subnet_mask,gate_way,dns,mac_address};
					}else {
						statusinfo  = new String[] {null,null,null,null,mac_address};
					}
					
	            }else if(ethernet_dev_status.equals("up")&&
	            	info.getConnectMode().equals(EthernetDevInfo.ETH_CONN_MODE_MANUAL)){
					Log.d(TAG,"----------connected mode:manual");
					if (isIpV4Address(info.getIpAddress())) {
					ip_address = info.getIpAddress();
	            	subnet_mask = info.getNetMask();
	            	gate_way = info.getRouteAddr() ;
	            	dns = info.getDnsAddr();
	            	statusinfo  = new String[] {ip_address,subnet_mask,gate_way,dns,mac_address};
					}else {
						statusinfo  = new String[] {null,null,null,null,mac_address};
					}
					
				}
				else {
	            	statusinfo  = new String[] {null,null,null,null,mac_address};
	    		}
	            
	        } else if (TAB_PPOE.equalsIgnoreCase(tabId)) {
	        	mac_address = readMacAddress();
	        	statusinfo  = new String[] {ip_address,subnet_mask,gate_way,dns,mac_address};
	            
	        } else if (TAB_IPV6.equalsIgnoreCase(tabId)) {
	            mac_address = readMacAddress();
	            if (!isIpV4Address(Utils.formatIpv6Addresses(getActivity()))) {
	            ip_address = Utils.formatIpv6Addresses(getActivity());
	            gate_way = Utils.formatGateway(getActivity());
	            statusinfo  = new String[] {ip_address,gate_way,mac_address};
				}else {
					statusinfo  = new String[] {null,null,mac_address};
				}
	            
	            
	        } else {
	            // Invalid option. Do nothing
	           // return;
	        }
	        return  statusinfo;
	    }
		
	   private String readMacAddress(){
		   String mMac_address=null;
		   FileReader fileReader = null;
           try
           {
                   fileReader = new FileReader(MAC_ADDRESS);
           }catch (FileNotFoundException e)
           {
                   e.printStackTrace();
           }
           BufferedReader bufferedReader = null;
           bufferedReader = new BufferedReader(fileReader);
           try
           {
                   mMac_address = bufferedReader.readLine();
           }catch (IOException e)
           {
                   e.printStackTrace();
           }                    
           try
           {
                   bufferedReader.close();
           }catch (IOException e)
           {
                   e.printStackTrace();
           }          
           try
           {
                   fileReader.close();
           }catch (IOException e)
           {
                   e.printStackTrace();
           }
           return mMac_address;
		   
	   }


	   private String getEthernetDevStatus(){
		   String mEhernet_dev_status=null;
		   FileReader fileReader = null;
           try
           {
                   fileReader = new FileReader(ETHERNET_DEV_STATUS);
           }catch (FileNotFoundException e)
           {
                   e.printStackTrace();
           }
           BufferedReader bufferedReader = null;
           bufferedReader = new BufferedReader(fileReader);
           try
           {
                   mEhernet_dev_status = bufferedReader.readLine();
           }catch (IOException e)
           {
                   e.printStackTrace();
           }                    
           try
           {
                   bufferedReader.close();
           }catch (IOException e)
           {
                   e.printStackTrace();
           }          
           try
           {
                   fileReader.close();
           }catch (IOException e)
           {
                   e.printStackTrace();
           }
           return mEhernet_dev_status;
		   
	   }
	   private String getWifiMacAddress(){

		        WifiManager wifiManager = (WifiManager) getActivity().getSystemService(getActivity().WIFI_SERVICE);
		        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		        String macAddress = wifiInfo == null ? null : wifiInfo.getMacAddress();  
		        return macAddress;
	   }
	   private String getIpv4Ipaddress(){
		   String ipaddress=null;
		   ipaddress =  Utils.getDefaultIpAddresses(getActivity());
		   return ipaddress;   
	   }
	   private String getWifiIpaddress(){
		   String ipaddress=null;
		   ipaddress =  Utils.getWifiIpAddresses(getActivity());
		   return ipaddress;   
	   }
	   private String getIpv6IpAddress(){
		   String ipv6Address = Utils.formatIpv6Addresses(getActivity());
		   return ipv6Address;
	   }
	   private String getIpv6IpGateWay(){
		   String ipv6GateWay = Utils.formatGateway(getActivity());
		   return ipv6GateWay;
	   }
	   
	   private boolean isIpV4Address(String value) {
            if(value==null)
                return false;
	        int start = 0;
	        int end = value.indexOf('.');
	        int numBlocks = 0;

	        while (start < value.length()) {
	            if (end == -1) {
	                end = value.length();
	            }

	            try {
	                int block = Integer.parseInt(value.substring(start, end));
	                if ((block > 255) || (block < 0)) {
	                        return false;
	                }
	            } catch (NumberFormatException e) {
	                    return false;
	            }

	            numBlocks++;

	            start = end + 1;
	            end = value.indexOf('.', start);
	        }
	        return numBlocks == 4;
	    }


	private class NetStatusAdapter extends BaseAdapter{
		String [] netstatusname;
		String [] netstatusinfo;
		private LayoutInflater mInflater;
		public NetStatusAdapter(Context context,String [] nane,String [] info){
			mInflater = LayoutInflater.from(context);
			netstatusname=nane;
			netstatusinfo=info;
			
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return netstatusname.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return netstatusname[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextViewHoder textViewHoder = new TextViewHoder();
			  convertView = mInflater.inflate(R.layout.list_content, null);

              // Creates a ViewHolder and store references to the two children views
              textViewHoder.statusname = (TextView) convertView.findViewById(R.id.status_name);
              textViewHoder.statusinfo = (TextView) convertView.findViewById(R.id.status_info);
              textViewHoder.statusname.setText(netstatusname[position]);
  			  textViewHoder.statusinfo.setText(netstatusinfo[position]);
  			  convertView.setTag(textViewHoder);
			return convertView;
		}
		
	}
	

	@Override
	public View createTabContent(String tag) {
		// TODO Auto-generated method stub
		return mRootView;
	}
	
   
}


