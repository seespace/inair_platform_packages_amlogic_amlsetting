package com.aml.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.ComponentName;
import android.os.Message;
import android.util.Log;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.WindowManagerPolicy;
import android.os.Message;
import android.app.SystemWriteManager;

public class BootReceiver extends BroadcastReceiver {
	private final String TAG = "BootReceiver";
	public static final String BOOTACTION = "android.intent.action.BOOT_COMPLETED";
	private static final String mAutoLanguagePreferencesFile = "cec_language_preferences";
	private static final String PROP_CEC_LANGUAGE_AUTO_SWITCH = "auto.swtich.language.by.cec";
	private static final String STR_KEEP_LANDSCAPE_IN_LAUNCHER = "keep.landscape.in.launcher2";
	private static final String PREFERENCE_KEEP_LANDSCAPE = "preference_keep_landscape_in_launcher";
	public static Boolean mBootCompleteStatus = false;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "intent.getAction(): " + intent.getAction());
		if (BOOTACTION.equals(intent.getAction())) {

			SharedPreferences sharedpreference = context.getSharedPreferences(
					mAutoLanguagePreferencesFile, Context.MODE_PRIVATE);
			Boolean lan = sharedpreference.getBoolean(
					PROP_CEC_LANGUAGE_AUTO_SWITCH, false);

			if (lan) {
				Log.d(TAG, "start checking service now");
				Intent serviceIntent = new Intent(context,CheckingService.class);
				serviceIntent.setAction("CEC_LANGUAGE_AUTO_SWITCH");
				context.startService(serviceIntent);
			} else {
				Log.d(TAG, "don't need start checking service ");
			}

            SystemWriteManager sw =(SystemWriteManager) context.getSystemService("system_write");
            SharedPreferences mSPKeepLandscpe = context.getSharedPreferences(
					PREFERENCE_KEEP_LANDSCAPE,Context.MODE_PRIVATE);
            String mKeepLandscape = mSPKeepLandscpe.getString(STR_KEEP_LANDSCAPE_IN_LAUNCHER, "true");
            sw.setProperty("sys.keeplauncher.landcape",mKeepLandscape);
            Log.d(TAG, "----boot complete:"+mKeepLandscape);
            
            if(Config.isMBXDevice()){
                HDMICheckHandler handler = new HDMICheckHandler(context, false);
                Message msg = handler.obtainMessage(HDMICheckHandler.HDMICHECK_START);
                msg.arg1 = HDMICheckHandler.EXECUTE_ONCE;
                handler.sendMessage(msg);
                mBootCompleteStatus = true;
                Log.d(TAG,"send HDMI check message...");
            }
            

		}else if(intent.getAction().equals(WindowManagerPolicy.ACTION_HDMI_HW_PLUGGED)){
            boolean plugged = intent.getBooleanExtra(WindowManagerPolicy.EXTRA_HDMI_HW_PLUGGED_STATE, false); 
            if(plugged)//HDMI plugged in
            {
                Log.d(TAG,"HDMI plugged");
                if(Config.isMBXDevice())
                {
                    HDMICheckHandler handler = new HDMICheckHandler(context, false);
                    Message msg = handler.obtainMessage(HDMICheckHandler.HDMICHECK_START);
                    msg.arg1 = HDMICheckHandler.EXECUTE_ONCE;
                    handler.sendMessage(msg);
                }
            }
            else{
                Log.d(TAG,"HDMI unPlugged");
                if(PreferenceWithHeaders.isMbxUI()){
                    HDMICheckHandler handler = new HDMICheckHandler(context, false);
                    Message msg = handler.obtainMessage(HDMICheckHandler.HDMICHECK_UNPLUGGED);
                    msg.arg1 = HDMICheckHandler.EXECUTE_UNPLUGGED;
                    handler.sendMessage(msg);
                    
                }
            }
    }

	}
}
