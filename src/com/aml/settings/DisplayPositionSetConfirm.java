package com.aml.settings;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

import java.util.ArrayList;

//import android.app.admin.DevicePolicyManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.app.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;

public class DisplayPositionSetConfirm extends Activity {
	private static final String TAG = "DisplayPositionSetConfirm";

	/** If there is output mode option, use this. */

	private AlertDialog DisplayPositionSetConfirmDiag = null;
	private Handler mWaitHandler;
	private final static long set_delay = 15 * 1000;
	private Handler mProgressHandler;
	private String messages = "";
	private int get_operation = 1;
	private static final int GET_USER_OPERATION = 1;
	private static final int GET_DEFAULT_OPERATION = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Bundle bundle = new Bundle();
		bundle = this.getIntent().getExtras();
		get_operation = bundle.getInt("get_operation");

		if (get_operation == GET_USER_OPERATION) {
			messages = getResources().getString(R.string.display_position_set_confirm_dialog_noreboot);
		} else if (get_operation == GET_DEFAULT_OPERATION) {
			messages = getResources().getString(R.string.display_position_set_default_confirm_dialog_noreboot);
		}
		showDispmodeSetMsg();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void showDispmodeSetMsg() {

		DisplayPositionSetConfirmDiag = new AlertDialog.Builder(this)
				.setTitle(R.string.display_position_dialog_title)
				.setMessage(messages)
				.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								setResult(RESULT_OK, null);
								finish();
							}
						})
				.setNegativeButton(R.string.display_position_dialog_no,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								setResult(RESULT_CANCELED, null);
								finish();
							}
						})
				.setOnKeyListener(new DialogInterface.OnKeyListener() {
					@Override
					public boolean onKey(DialogInterface dialog, int keyCode,
							KeyEvent event) {
						if (keyCode == KeyEvent.KEYCODE_BACK
								&& event.getAction() == KeyEvent.ACTION_DOWN
								&& !event.isCanceled()
								&& DisplayPositionSetConfirmDiag.isShowing()) {
							dialog.cancel();
							setResult(RESULT_CANCELED, null);
							finish();
							return true;
						}
						return false;
					}
				}).show();
	}
}
