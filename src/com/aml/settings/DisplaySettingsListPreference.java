package com.aml.settings;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.util.Log;

public class DisplaySettingsListPreference extends ListPreference {
	Context ct = null;
    private static final String TAG = "DisplaySettingsListPreference";

	public DisplaySettingsListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		ct = context;
	}

	public DisplaySettingsListPreference(Context context) {
		super(context);
		ct = context;
	}

	public void setIndex() {
		String currentMode = Config.getCurrentOutputResolution(ct);
		Config.Logd(getClass().getName(), "currentMode is: " + currentMode);

		CharSequence entries[] = getEntries();

		int index = 0;
		for (index = 0; index < entries.length; index++) {
			String entry = Config.Display2LogicEntry(entries[index] + "");

			Config.Logd(getClass().getName(), "entry is: " + entry);

			if (currentMode.equalsIgnoreCase(entry)) {
				break;
			}
		}

		if (index < 0 || index >= entries.length) {
			if (ct != null)
                if(DisplayFragment.isDongle){
                    index = ct.getResources().getInteger(
						R.integer.outputmode_default_values_dongle);
                }else{
                    index = ct.getResources().getInteger(
						R.integer.outputmode_default_values);
                }
		}
		if (index < 0 || index >= entries.length) {
			index = 0;
		}
		setValueIndex(index);
	}

	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		setIndex();
		super.onPrepareDialogBuilder(builder);
	}
}
